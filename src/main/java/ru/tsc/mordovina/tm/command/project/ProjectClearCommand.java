package ru.tsc.mordovina.tm.command.project;

import ru.tsc.mordovina.tm.command.AbstractProjectCommand;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return "project-clear";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Drop all projects";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        serviceLocator.getProjectService().clear(userId);
    }

}
