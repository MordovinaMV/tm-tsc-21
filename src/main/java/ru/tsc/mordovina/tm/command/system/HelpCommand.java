package ru.tsc.mordovina.tm.command.system;

import ru.tsc.mordovina.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return "help";
    }

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Display list of commands";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands())
            System.out.println(command.toString());
    }

}
