package ru.tsc.mordovina.tm.command;

import ru.tsc.mordovina.tm.model.User;

import java.util.Optional;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(final User user) {
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println(
                "Name: " +
                user.getLastName() + " " +
                user.getFirstName() + " " +
                user.getMiddleName()
        );
        System.out.println("Email: " + user.getEmail());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

}
