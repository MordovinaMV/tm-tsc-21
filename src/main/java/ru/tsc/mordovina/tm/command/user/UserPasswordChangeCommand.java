package ru.tsc.mordovina.tm.command.user;

import ru.tsc.mordovina.tm.command.AbstractUserCommand;
import ru.tsc.mordovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.mordovina.tm.exception.system.AccessDeniedException;
import ru.tsc.mordovina.tm.model.User;
import ru.tsc.mordovina.tm.util.TerminalUtil;

import java.util.Optional;

public class UserPasswordChangeCommand extends AbstractUserCommand {

    @Override
    public String getCommand() {
        return "user-change-password";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Changes user password";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        if (!isAuth) throw new AccessDeniedException();
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

}
