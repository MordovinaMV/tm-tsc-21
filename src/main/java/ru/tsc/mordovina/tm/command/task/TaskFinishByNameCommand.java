package ru.tsc.mordovina.tm.command.task;

import ru.tsc.mordovina.tm.command.AbstractTaskCommand;
import ru.tsc.mordovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.mordovina.tm.model.Task;
import ru.tsc.mordovina.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskFinishByNameCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return "task-finish-by-name";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Finish task by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findByName(userId, name);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        serviceLocator.getTaskService().finishByName(userId, name);
    }

}
