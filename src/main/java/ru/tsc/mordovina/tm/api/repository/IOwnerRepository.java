package ru.tsc.mordovina.tm.api.repository;

import ru.tsc.mordovina.tm.api.IRepository;
import ru.tsc.mordovina.tm.model.AbstractOwnerEntity;
import ru.tsc.mordovina.tm.model.Project;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E add(String userId, E entity);

    E findById(String userId, String id);

    E findByIndex(String userId, Integer index);

    void clear(String userId);

    E removeById(String userId, String id);

    E removeByIndex(String userId, Integer index);

    void remove(String userId, E entity);

    Integer getSize(String userId);

}
