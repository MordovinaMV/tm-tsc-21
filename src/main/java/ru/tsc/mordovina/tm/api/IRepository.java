package ru.tsc.mordovina.tm.api;

import ru.tsc.mordovina.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    void add(final E entity);

    void remove(final E entity);

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    boolean existsById(final String id);

    boolean existsByIndex(final Integer index);

    void clear();

    E findById(final String id);

    E findByIndex(final Integer index);

    E removeById(final String id);

    E removeByIndex(final Integer index);

    Integer getSize();

}
