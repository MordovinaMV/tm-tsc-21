package ru.tsc.mordovina.tm.api.repository;

import ru.tsc.mordovina.tm.api.IRepository;
import ru.tsc.mordovina.tm.model.User;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    User findUserByLogin(String login);

    User findUserByEmail(String email);

    User removeUserById(String id);

    User removeUserByLogin(String login);

    boolean userExistsByLogin(String login);

    boolean userExistsByEmail(String email);

}
