package ru.tsc.mordovina.tm.api.service;

public interface ServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();

}
