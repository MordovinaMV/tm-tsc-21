package ru.tsc.mordovina.tm.service;

import ru.tsc.mordovina.tm.api.repository.IProjectRepository;
import ru.tsc.mordovina.tm.api.repository.ITaskRepository;
import ru.tsc.mordovina.tm.api.service.IProjectTaskService;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    public Task bindTaskById(final String userId, final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public Task unbindTaskById(final String userId, final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return null;
        return taskRepository.unbindTaskById(userId, taskId);
    }

    @Override
    public Project removeById(final String userId, final String projectId) {
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        Project project = projectRepository.findByIndex(index);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        Project project = projectRepository.findByName(userId, name);
        String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

}
